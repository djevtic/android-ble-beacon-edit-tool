package beaconadmin.com.beaconadmin;

import android.app.Activity;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by djevt_000 on 8/2/2015.
 */
public class BeaconListAdapter extends ArrayAdapter {

    private Context mContext;
    private Activity mActivity;
    private ArrayList<ScanResult> mResults = new ArrayList<>();

    static class ViewHolder {
        public TextView textUID;
        public TextView textProducer;
    }


    public BeaconListAdapter(Activity activity, Context context, ArrayList<ScanResult> results) {
        super(context, R.layout.beacon_row_layout, results);
        mActivity = activity;
        mContext = context;
        mResults = results;
    }

    @Override
    public int getCount() {
        return mResults.size();
    }

    @Override
    public Object getItem(int i) {
        return mResults.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if(rowView == null) {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE); //mActivity.getLayoutInflater();
            rowView = inflater.inflate(R.layout.beacon_row_layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.textUID = (TextView) rowView.findViewById(R.id.beacon_row_uid);
            viewHolder.textProducer = (TextView) rowView.findViewById(R.id.beacon_id_producer);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        if (!mResults.isEmpty()) {
            ScanResult result = mResults.get(position);
            holder.textUID.setText(String.valueOf(result.getDevice().getAddress()));
            holder.textProducer.setText(result.getScanRecord().getDeviceName());
        }

        return rowView;
    }
}
